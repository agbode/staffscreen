package com.chamsaccess.sellermealmanagement;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.view.View;

public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }



   public void ChangeFragment(View view){
    Fragment fragment;

    if (view == findViewById(R.id.dashboard)){
        fragment = new DashboardFragment();
        FragmentManager dashboardManager = getFragmentManager();
        FragmentTransaction dashboardTransaction = dashboardManager.beginTransaction();
        dashboardTransaction.replace(R.id.dashboard_fragment, fragment);
        dashboardTransaction.commit();
    }
      /* if (view == findViewById(R.id.food_category)){
           fragment = new FoodCategoryFragment();
           FragmentManager dashboardManager = getFragmentManager();
           FragmentTransaction dashboardTransaction = dashboardManager.beginTransaction();
           dashboardTransaction.replace(R.id.dashboard_fragment, fragment);
           dashboardTransaction.commit();
       }
       if (view == findViewById(R.id.meal_inventory)){
           fragment = new MealInventoryFragment();
           FragmentManager dashboardManager = getFragmentManager();
           FragmentTransaction dashboardTransaction = dashboardManager.beginTransaction();
           dashboardTransaction.replace(R.id.dashboard_fragment, fragment);
           dashboardTransaction.commit();

       }
       if (view == findViewById(R.id.view_existing)){
           fragment = new ViewExistingFragment();
           FragmentManager dashboardManager = getFragmentManager();
           FragmentTransaction dashboardTransaction = dashboardManager.beginTransaction();
           dashboardTransaction.replace(R.id.dashboard_fragment, fragment);
           dashboardTransaction.commit();

       }
       if (view == findViewById(R.id.existing_meal)){
           fragment = new ExistingMealFragment();
           FragmentManager dashboardManager = getFragmentManager();
           FragmentTransaction dashboardTransaction = dashboardManager.beginTransaction();
           dashboardTransaction.replace(R.id.dashboard_fragment, fragment);
           dashboardTransaction.commit();

       }*/
    }
}
